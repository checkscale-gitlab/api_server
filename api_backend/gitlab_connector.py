import json
import gitlab
import yaml

from private_config.variables import gitlab_user


class GitlabConnector:

    gl = None
    gitlab_cfg = None
    gitlab_id = None
    file_name = None

    app_data = None
    ci_definition = None
    project_name = None

    def __init__(self, gitlab_id, gitlab_cfg, analysis_definition_file=None):

        self.gitlab_id = gitlab_id
        self.gitlab_cfg = gitlab_cfg
        self.gl = self.read_gitlab_cfg()

        if analysis_definition_file is not None:
            self.file_name = analysis_definition_file
            self.app_data = self.open_ci_definition(self.file_name, 'dict')
            # self.ci_definition = self.open_ci_definition(self.file_name, 'string')

    def read_gitlab_cfg(self):
        return gitlab.Gitlab.from_config(self.gitlab_id, self.gitlab_cfg)

    def lint_yaml(self, the_ci_definition):
        if isinstance(the_ci_definition, str):
            return self.gl.lint(the_ci_definition)
        if isinstance(the_ci_definition, dict):
            return self.gl.lint(json.dumps(the_ci_definition))

    @staticmethod
    def open_ci_definition(the_filename, type_output='string'):
        with open(the_filename) as file:
            # The FullLoader parameter handles the conversion from YAML
            # scalar values to Python the dictionary format
            tmp_dict = yaml.safe_load(file)
        if type_output == 'string':
            return json.dumps(tmp_dict)
        if type_output == 'dict':
            return tmp_dict

    def get_app_name(self):
        try:
            return self.app_data.get("variables").get("APP_NAME")
        except AttributeError as ex:
            print("get_app_name: {ex}".format(ex=ex))

    def get_group_name(self):
        try:
            return self.app_data.get("variables").get("GROUP_NAME")
        except AttributeError as ex:
            print("get_group_name: {ex}".format(ex=ex))

    def get_project_name(self):
        try:
            return self.project_name
        except AttributeError as ex:
            print("get_project_name: {ex}".format(ex=ex))

    def get_git_url(self):
        try:
            return self.app_data.get("variables").get("GIT_URL")
        except AttributeError as ex:
            print("get_git_url: {ex}".format(ex=ex))

    def get_branch_name(self):
        try:
            return self.app_data.get("variables").get("BRANCH_NAME")
        except AttributeError as ex:
            print("get_branch_name: {ex}".format(ex=ex))

    def get_git_depth(self):
        try:
            return self.app_data.get("variables").get("GIT_DEPTH")
        except AttributeError as ex:
            print("get_git_depth: {ex}".format(ex=ex))

    def get_env(self):
        try:
            return self.app_data.get("variables").get("ENV")
        except AttributeError as ex:
            print("get_env: {ex}".format(ex=ex))

    def get_global_variable(self, variable_name):
        """
            'variable_name' is the name inside global variables at Security-Analysis.gitlab-ci.yml
            examples of global variables are 'APP_NAME', 'GROUP_NAME', 'GIT_URL', 'ENV'

            :param variable_name:
            :return: None if app_data is unavailable else 'variable_name' selected
        """
        return self.app_data.get("variables").get(variable_name)

    def get_stages(self):
        return self.app_data.get("stages")

    # Groups
    def create_group(self, the_name, the_path):
        try:
            self.gl.groups.create({'name': the_name, 'path': the_path})
            # return gp_details.get('visibility'), gp_details.get('archived'), gp_details.get('enabled')
        except gitlab.exceptions.GitlabCreateError as create_group_ex:
            print("create_group_ex: {}".format(create_group_ex))
            exit(1)

    def list_groups(self, ):
        return self.gl.groups.list()

    def group_details(self, the_group_name):
        return self.gl.groups.get(the_group_name)

    def get_group_id(self, the_group_name):
        try:
            return self.gl.groups.get(the_group_name).id
        except gitlab.exceptions.GitlabGetError as group_id_ex:
            print("group_id_ex: {}".format(group_id_ex))
            return None

    def group_membership(self, the_group_name, the_username):
        the_group = self.gl.groups.get(self.get_group_id(the_group_name))
        the_user_details = self.gl.users.list(search=the_username)

        print("group: {}".format(the_group))
        print("user_details: {}".format(the_user_details))

        try:
            the_members = the_group.members.get(the_user_details[0].id)
            print("members: {}".format(the_members))
            return True
        except Exception as members_ex:
            print("members_ex: {}".format(members_ex))
            return False

    @staticmethod
    def update_group(the_group, the_description):
        the_group.description = the_description
        the_group.save()
        return the_group

    # Projects
    def list_projects(self):
        all_the_projects = self.gl.projects.list()
        for project in all_the_projects:
            for my_method in [f for f in dir(project) if not f.startswith('_')]:
                print(my_method)
        return all_the_projects

    def create_project_in_group(self, the_group_id, the_repo):
        try:
            self.gl.projects.create({'name': the_repo, 'namespace_id': the_group_id})
        except gitlab.GitlabCreateError as create_project_ex:
            print("create_project_ex: {}".format(create_project_ex))

    def get_project_id(self, the_group_name, the_project_name):
        try:
            the_group_project = "{group}/{project}".format(group=the_group_name, project=the_project_name)  # 'my_team/my_project'
            return self.gl.projects.get(the_group_project).id
        except gitlab.exceptions.GitlabGetError as project_id_ex:
            print("{ex} - project: {prj}".format(prj=the_group_project, ex=project_id_ex))
            return None

    def get_project(self, the_group_name, the_project_name):
        try:
            the_group_project = "{group}/{project}".format(group=the_group_name, project=the_project_name)  # 'my_team/my_project'
            return self.gl.projects.get(self.gl.projects.get(the_group_project).id)
        except gitlab.exceptions.GitlabGetError as get_project_ex:
            print("project: {prj}, get_project: {ex}".format(prj=the_group_project, ex=get_project_ex))
            return None

    def create_workgroup_and_project(self, the_workgroup, the_project_name):

        # Groups at GitLab is a similar concept to Units/Divisions/Sections/Departments/Teams/WorkGroups/WorkProjects
        # - Gitlab 'project' equals to a code repository
        # - GitLab 'group' contains several code repositories
        workgroup = workgroup_path = the_workgroup
        self.project_name = the_project_name
        print("workgroup: {grp}".format(grp=workgroup))
        print("project: {prj}".format(prj=self.project_name))

        # Create Group
        if self.get_group_id(workgroup) is None:
            self.create_group(workgroup, workgroup_path)

        if self.get_group_id(workgroup) is not None:
            members = self.group_membership(workgroup, gitlab_user)
            print("members: {}".format(members))

        # Check if project exist
        try:
            project_id = self.get_project_id(workgroup, self.project_name)
        except Exception as ex:
            print("ex: {ex}".format(ex=ex))

        # Create project
        if project_id is None and members is True:
            group_id = self.get_group_id(workgroup)
            self.create_project_in_group(group_id, self.project_name)
            project_id = self.get_project_id(workgroup, self.project_name)
            print("project exists: {prj}, {id}".format(prj=self.project_name, id=project_id))

    @staticmethod
    def retrieve_auto_devops(the_project):
        return the_project.auto_devops_enabled

    @staticmethod
    def set_auto_devops(the_project, the_value=True):
        the_project.auto_devops_enabled = the_value

    @staticmethod
    def auto_devops_deploy_strategy(the_project):
        return the_project.auto_devops_deploy_strategy