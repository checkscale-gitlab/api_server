openapi: 3.0.0
info:
  title: SecureApps@CI - API
  description: API to enable Application Security integration on CI/CD Pipelines.
  license:
    name: GPLv3
    url: https://www.gnu.org/licenses/gpl-3.0.en.html
  version: v1
servers:
- url: https://virtserver.swaggerhub.com/ema.rainho/secureapps-ci/v1
  description: SwaggerHub API Auto Mocking
- url: https://localhost:5555/ema.rainho/secureapps-ci/v1
  description: Sandbox server (uses test data)
security:
- ApiKeyAuth: []
paths:
  /health:
    get:
      summary: Returns the API health status
      operationId: health_get
      responses:
        "200":
          description: API Health Status response (up/down)
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200'
      security: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /version:
    get:
      summary: Returns the API version
      operationId: version_get
      responses:
        "200":
          description: API version number response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_1'
      security: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /analysis/create:
    post:
      summary: Create a new Analysis according to the uploaded definition
      operationId: analysis_create_post
      requestBody:
        content:
          application/octet-stream:
            schema:
              properties:
                filename:
                  type: string
                  format: binary
      responses:
        '101':
          description: Continue - The Analysis request has been received and is processing, but no response is available yet.
        '102':
          description: Processing - The Analysis is OK so far, client should continue the request or ignore the response if the request already finished
        "200":
          description: Returns new analysis name, analysisId and accepted code
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AnalysisCreate'
        "400":
          description: The specified YAML definition is invalid (e.g. not a valid
            YAML)
        "401":
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              style: simple
              explode: false
              schema:
                type: string
        "404":
          description: The YAML definition for a new analysis was not found
      security:
      - ApiKeyAuth: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /analysis/{analysisId}/abort:
    delete:
      summary: Abort a Analysis by ID
      operationId: analysis_analysis_id_abort_delete
      parameters:
      - name: analysisId
        in: path
        description: The ID of the Analysis to abort
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: Returns ID of analysis to abort and abort_status
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AnalysisAbort'
      security:
      - ApiKeyAuth: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /analysis/{analysisId}/progress:
    get:
      summary: The Progress of Analysis Stages 1,2,3 and Tool jobs
      operationId: analysis_analysis_id_progress_get
      parameters:
      - name: analysisId
        in: path
        description: The ID of the Analysis to retrieve progress
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: Analysis progress response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AnalysisProgress'
        "400":
          description: The specified analysisId is invalid (e.g. not a number)
        "401":
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              style: simple
              explode: false
              schema:
                type: string
        "404":
          description: The analysis with the specified ID was not found
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /analysis/{analysisId}/stage/{stageName}/jobs:
    get:
      summary: Returns Jobs for a specific stage
      operationId: analysis_analysis_id_stage_stage_name_jobs_get
      parameters:
      - name: analysisId
        in: path
        description: The ID of the Analysis to retrieve Stages
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      - name: stageName
        in: path
        description: The ID of the Stage to return Jobs
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        "200":
          description: Returns Jobs from a Stage in analysisId.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_2'
        "400":
          description: The specified analysisId or stageName is invalid (e.g. not
            a number or a name)
        "401":
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              style: simple
              explode: false
              schema:
                type: string
        "404":
          description: The Analysis or Stage with the specified IDs was not found
      security:
      - ApiKeyAuth: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /analysis/{analysisId}/results:
    get:
      summary: Returns results status of Analysis (can include report if complete)
      operationId: analysis_analysis_id_results_get
      parameters:
      - name: analysisId
        in: path
        description: The ID of the Analysis to retrieve results
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: Analysis Status response (can include Report results if finished)
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AnalysisResults'
        "400":
          description: The specified analysisId is invalid (e.g. not a number)
        "401":
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              style: simple
              explode: false
              schema:
                type: string
        "404":
          description: The Analysis with the specified ID was not found
      security:
      - ApiKeyAuth: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /tool/create:
    post:
      summary: Create a new Tool according to the uploaded definition
      operationId: tool_create_post
      requestBody:
        content:
          application/octet-stream:
            schema:
              type: string
              format: binary
      responses:
        "200":
          description: Returns new Tool name, toolId and accepted code
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ToolCreate'
        "400":
          description: The specified Dockerfile definition is invalid (e.g. not a
            valid Dockerfile)
        "401":
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              style: simple
              explode: false
              schema:
                type: string
        "404":
          description: The Dockerfile definition for a new Tool was not found
      security:
      - ApiKeyAuth: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /tool/{toolId}/remove:
    delete:
      summary: Remove the specified Security Tool
      operationId: tool_tool_id_remove_delete
      parameters:
      - name: toolId
        in: path
        description: The ID of the tool (name:tag)
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        "200":
          description: Returns ID of toolId to remove and delete_status
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ToolRemove'
        "400":
          description: The specified toolId is invalid (e.g. not a number)
        "401":
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              style: simple
              explode: false
              schema:
                type: string
        "404":
          description: The Tool with the specified ID was not found
      security:
      - ApiKeyAuth: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /tool/{toolId}/details:
    get:
      summary: Returns details of specific Security Tool
      operationId: tool_tool_id_details_get
      parameters:
      - name: toolId
        in: path
        description: The ID of the tool (name:tag)
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        "200":
          description: The Dockerfile of the Security Tool with identified by toolId
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ToolDetails'
        "400":
          description: The specified toolId is invalid (e.g. not a number)
        "401":
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              style: simple
              explode: false
              schema:
                type: string
        "404":
          description: The Tool with the specified ID was not found
      security:
      - ApiKeyAuth: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
  /tools/list:
    get:
      summary: Collect available Security Tools
      operationId: tools_list_get
      responses:
        "200":
          description: Returns a list of Security Tools
          content:
            application/json:
              schema:
                uniqueItems: true
                type: array
                example:
                - tool_id: truffleHog:v2
                  tool_name: truffleHog
                  tool_type: secret-detection
                  tool_definition: dockerfile
                - tool_id: sonar-scanner:4.4
                  tool_name: sonar-scanner-cli
                  tool_type: sast
                  tool_definition: bash
                - tool_id: zap2docker-live:latest
                  tool_name: zap2docker-live
                  tool_type: dast
                  tool_definition: dockerfile
                items:
                  $ref: '#/components/schemas/Tool'
                x-content-type: application/json
        "401":
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              style: simple
              explode: false
              schema:
                type: string
        "404":
          description: No Security Tools found!
      security:
      - ApiKeyAuth: []
      x-openapi-router-controller: swagger_server.controllers.default_controller
components:
  schemas:
    Analysis:
      required:
      - analysis_id
      type: object
      properties:
        analysis_id:
          type: string
        analysis_name:
          type: string
      additionalProperties: false
    AnalysisCreate:
      allOf:
      - $ref: '#/components/schemas/Analysis'
      - required:
        - accepted
        type: object
        properties:
          accepted:
            type: boolean
        additionalProperties: false
    AnalysisAbort:
      allOf:
      - $ref: '#/components/schemas/Analysis'
      - required:
        - abort_status
        type: object
        properties:
          abort_status:
            type: string
            enum:
            - success
            - fail
        additionalProperties: false
    AnalysisProgress:
      allOf:
      - $ref: '#/components/schemas/Analysis'
      - required:
        - progress
        type: object
        properties:
          progress:
            type: string
            enum:
            - accepted
            - ongoing
            - completed
          stages:
            uniqueItems: true
            type: array
            items:
              $ref: '#/components/schemas/Stage'
        additionalProperties: false
    AnalysisResults:
      allOf:
      - $ref: '#/components/schemas/Analysis'
      - required:
        - analysis_status
        - report_id
        - report_results
        type: object
        properties:
          analysis_status:
            type: string
          report_id:
            type: integer
          report_results:
            type: object
        additionalProperties: false
    Stage:
      type: object
      properties:
        stage_name:
          type: string
        stage_status:
          type: string
      additionalProperties: false
    Tool:
      type: object
      properties:
        tool_id:
          type: string
        tool_name:
          type: string
      additionalProperties: false
    ToolCreate:
      allOf:
      - $ref: '#/components/schemas/Tool'
      - required:
        - accepted
        type: object
        properties:
          accepted:
            type: boolean
        additionalProperties: false
    ToolRemove:
      allOf:
      - $ref: '#/components/schemas/Tool'
      - required:
        - remove_status
        type: object
        properties:
          remove_status:
            type: string
            enum:
            - complete
            - fail
        additionalProperties: false
    ToolDetails:
      allOf:
      - $ref: '#/components/schemas/Tool'
      - required:
        - dockerfile_dump
        - dockerfile_id
        - tool_definition
        - tool_type
        type: object
        properties:
          tool_type:
            type: string
          tool_definition:
            type: string
            enum:
            - dockerfile
            - bash
          dockerfile_id:
            type: string
          dockerfile_dump:
            type: string
            format: binary
        additionalProperties: false
    Job:
      type: object
      properties:
        job_id:
          type: integer
          format: int64
        job_name:
          type: string
        job_type:
          type: string
        job_definition:
          type: string
      additionalProperties: false
      example:
        job_type: job_type
        job_name: job_name
        job_id: 0
        job_definition: job_definition
    inline_response_200:
      required:
      - api_status
      type: object
      properties:
        api_status:
          type: string
          enum:
          - up
          - down
      additionalProperties: false
      example:
        api_status: up
    inline_response_200_1:
      required:
      - api_version
      type: object
      properties:
        api_version:
          type: string
          enum:
          - v1
      additionalProperties: false
      example:
        api_version: v1
    inline_response_200_2:
      required:
      - analysis_id
      - job_list
      - stage_name
      type: object
      properties:
        analysis_id:
          type: string
        stage_name:
          type: string
        job_list:
          uniqueItems: true
          type: array
          items:
            $ref: '#/components/schemas/Job'
      additionalProperties: false
      example:
        stage_name: stage_name
        job_list:
        - job_type: job_type
          job_name: job_name
          job_id: 0
          job_definition: job_definition
        - job_type: job_type
          job_name: job_name
          job_id: 0
          job_definition: job_definition
        analysis_id: analysis_id
  responses:
    UnauthorizedError:
      description: API key is missing or invalid
      headers:
        WWW_Authenticate:
          style: simple
          explode: false
          schema:
            type: string
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      name: X-API-KEY
      in: header
      x-apikeyInfoFunc: swagger_server.controllers.authorization_controller.check_api_key_auth

