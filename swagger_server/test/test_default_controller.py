# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.analysis_abort import AnalysisAbort  # noqa: E501
from swagger_server.models.analysis_create import AnalysisCreate  # noqa: E501
from swagger_server.models.analysis_progress import AnalysisProgress  # noqa: E501
from swagger_server.models.analysis_results import AnalysisResults  # noqa: E501
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from swagger_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from swagger_server.models.tool import Tool  # noqa: E501
from swagger_server.models.tool_create import ToolCreate  # noqa: E501
from swagger_server.models.tool_details import ToolDetails  # noqa: E501
from swagger_server.models.tool_remove import ToolRemove  # noqa: E501
from swagger_server.test import BaseTestCase

response_body = 'Response body is : '


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_analysis_analysis_id_abort_delete(self):
        """Test case for analysis_analysis_id_abort_delete

        Abort a Analysis by ID
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/analysis/{analysis_id}/abort'.format(analysis_id=789),
            method='DELETE')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_analysis_analysis_id_progress_get(self):
        """Test case for analysis_analysis_id_progress_get

        The Progress of Analysis Stages 1,2,3 and Tool jobs
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/analysis/{analysis_id}/progress'.format(analysis_id=789),
            method='GET')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_analysis_analysis_id_results_get(self):
        """Test case for analysis_analysis_id_results_get

        Returns results status of Analysis (can include report if complete)
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/analysis/{analysis_id}/results'.format(analysis_id=789),
            method='GET')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_analysis_analysis_id_stage_stage_name_jobs_get(self):
        """Test case for analysis_analysis_id_stage_stage_name_jobs_get

        Returns Jobs for a specific stage
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/analysis/{analysis_id}/stage/{stage_name}/jobs'.format(analysis_id=789, stage_name='stage_name_example'),
            method='GET')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_analysis_create_post(self):
        """Test case for analysis_create_post

        Create a new Analysis according to the uploaded definition
        """
        body = AnalysisCreate()
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/analysis/create',
            method='POST',
            data=json.dumps(body),
            content_type='application/octet-stream')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_health_get(self):
        """Test case for health_get

        Returns the API health status
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/health',
            method='GET')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_tool_create_post(self):
        """Test case for tool_create_post

        Create a new Tool according to the uploaded definition
        """
        body = ToolCreate()
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/tool/create',
            method='POST',
            data=json.dumps(body),
            content_type='application/octet-stream')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_tool_tool_id_details_get(self):
        """Test case for tool_tool_id_details_get

        Returns details of specific Security Tool
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/tool/{tool_id}/details'.format(tool_id='tool_id_example'),
            method='GET')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_tool_tool_id_remove_delete(self):
        """Test case for tool_tool_id_remove_delete

        Remove the specified Security Tool
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/tool/{tool_id}/remove'.format(tool_id='tool_id_example'),
            method='DELETE')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_tools_list_get(self):
        """Test case for tools_list_get

        Collect available Security Tools
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/tools/list',
            method='GET')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))

    def test_version_get(self):
        """Test case for version_get

        Returns the API version
        """
        response = self.client.open(
            '/ema.rainho/secureapps-ci/v1/version',
            method='GET')
        self.assert200(response,
                       response_body + response.app_data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
