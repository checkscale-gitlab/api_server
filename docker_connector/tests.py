import logging

from docker_connector.actions import DockerActions

if __name__ == '__main__':

    # sample container
    image_name = 'alpine'
    image_tag = 'latest'
    container_args1 = ["echo", "hello", "world"]
    container_args2 = ["tail", "-f", "/dev/null"]
    dockerfiles_path = './dockerfiles/'

    # client for communicating with a Docker server.
    docker_action = DockerActions()

    # containers to launch
    containers_list = [
        {'container_name': 'ctr-fail',
         'image': {
             'image_name': 'fail',
             'image_tag': 'latest',
             'dockerfile': 'Dockerfile.fail',
             'dockerfile_path': dockerfiles_path,
             'rm_intermediate': True,
             'command': 'whoami'
         }},
        {'container_name': 'ctr-error',
         'image': {
             'image_name': 'error',
             'image_tag': 'latest',
             'dockerfile': 'Dockerfile.error',
             'dockerfile_path': dockerfiles_path,
             'rm_intermediate': True,
             'command': 'whoami'
         }},
        {'container_name': 'ctr-success',
         'image': {
             'image_name': 'success',
             'image_tag': 'latest',
             'dockerfile': 'Dockerfile.success',
             'dockerfile_path': dockerfiles_path,
             'rm_intermediate': True,
             'command': 'whoami'
         }
         }
    ]

    # container with parameters
    container_with_parameters = {'container_name': 'ctr-fail',
                                 'image': {
                                     'image_name': 'fail',
                                     'image_tag': 'latest',
                                     'dockerfile': 'Dockerfile.fail',
                                     'dockerfile_path': dockerfiles_path,
                                     'rm_intermediate': True,
                                 },
                                 'container': {
                                     'name': '',  # str – The name for this container
                                     'entrypoint': None,  # str or list – The entrypoint for the container.
                                     'healthcheck': None,
                                     # dict – Specify a test to perform to check that the container is healthy.
                                     'user': 'appsec',
                                     # str or int – Username or UID to run commands as inside the container.
                                     'command': 'whoami',  # str or list – The command to run in the container
                                     'remove': True,  # bool – Remove the container when it has finished running.
                                     'network_disabled': True,  # bool – Disable networking.
                                     'dns': ['127.0.0.1'],  # list – Set custom DNS servers.
                                     'privileged': False,  # bool – Give extended privileges to this container.
                                     'publish_all_ports': False,  # bool – Publish all ports to the host.
                                     'cap_drop': ['CAP_ADMIN'],  # list of str – Drop kernel capabilities.
                                     'security_opt': ['no-new-privileges'],
                                     # A list of string values of custom labels for MLS systems (SELinux...)
                                 }}

    # clean start
    logging.info("stop all containers ...")
    docker_action.stop_all()
    docker_action.list_containers()

    logging.info("prune stopped containers ...")
    docker_action.prune()

    logging.info("list docker networks ...")
    docker_action.list_networks()

    logging.info("check images")
    docker_action.check_image('fail', 'latest')

    logging.info("building images fail, error, success")
    for container in containers_list:
        logging.info("building image '{}:{}' ... ".format(container['image']['image_name'], container['image']['image_tag']))
        docker_action.build_image(
            container['image']['dockerfile_path'],
            container['image']['dockerfile'],
            container['image']['image_name'],
            container['image']['image_tag'],
            container['image']['rm_intermediate']
        )

    # print("start container with image: {}...".format(image_name))
    # container_output = docker_action.run_container(image_name, image_tag, container_args1)
    # docker_action.list_containers()

    # print("show container logs ...")
    # print(docker_action.collect_logs(container_output['container_obj']))

    # print("stop container ...")
    # docker_action.stop_container(container_output['container_obj'])
    # docker_action.list_containers()

    docker_action.launch_in_parallel(containers_list)


